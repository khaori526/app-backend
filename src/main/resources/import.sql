INSERT INTO serie (title, description, premiereDate) VALUES ('Los backyardigans', 'Es una serie de discovery kids creo', '2019-08-11');
INSERT INTO serie (title, description, premiereDate) VALUES ('Los picapiedra', 'Daba en el canal 2', '2002-07-11');
INSERT INTO serie (title, description, premiereDate) VALUES ('Thundercats', 'Serie de culto', '1989-02-08');
INSERT INTO serie (title, description, premiereDate) VALUES ('La abeja maya', 'Serie q trata de una abejita q se llama maya', '1999-10-28');

INSERT INTO season (id) VALUES (1);
INSERT INTO season (id) VALUES (2);
INSERT INTO season (id) VALUES (3);
INSERT INTO season (id) VALUES (4);
INSERT INTO season (id) VALUES (5);
INSERT INTO season (id) VALUES (6);
INSERT INTO season (id) VALUES (7);

INSERT INTO season_serie (season_id, serie_id, episodes) VALUES (1, 1, 24);
INSERT INTO season_serie (season_id, serie_id, episodes) VALUES (2, 1, 14);
INSERT INTO season_serie (season_id, serie_id, episodes) VALUES (1, 2, 12);
INSERT INTO season_serie (season_id, serie_id, episodes) VALUES (2, 2, 10);
INSERT INTO season_serie (season_id, serie_id, episodes) VALUES (3, 2, 20);
INSERT INTO season_serie (season_id, serie_id, episodes) VALUES (1, 3, 16);
INSERT INTO season_serie (season_id, serie_id, episodes) VALUES (1, 4, 22);
INSERT INTO season_serie (season_id, serie_id, episodes) VALUES (2, 4, 19);