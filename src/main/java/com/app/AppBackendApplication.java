package com.app;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.app.entity.Serie;
import com.app.service.SerieService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class AppBackendApplication implements CommandLineRunner{
	
	@Autowired
	private SerieService serieService;

	public static void main(String[] args) {
		SpringApplication.run(AppBackendApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
//		Map<String, Object> response = new HashMap<>();
//		
//		Integer id = 2;
//		Serie serie = serieService.findById(id);
//		serie.setSeasons(serieService.findSerieWithSeasons(serie.getId()));
//		response.put("serie", serie);
//		
//		ObjectMapper mapper = new ObjectMapper();
//    	try {
//			String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response);
//			System.out.println(json);
//		} catch (JsonProcessingException e1) {
//			
//			e1.printStackTrace();
//		}
	}

}
