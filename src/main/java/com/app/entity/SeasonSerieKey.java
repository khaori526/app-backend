package com.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SeasonSerieKey implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "serie_id")
    private Integer serie_id;
 
    @Column(name = "season_id")
    private Integer season_id;

	public Integer getSerie_id() {
		return serie_id;
	}

	public void setSerie_id(Integer serie_id) {
		this.serie_id = serie_id;
	}

	public Integer getSeason_id() {
		return season_id;
	}

	public void setSeason_id(Integer season_id) {
		this.season_id = season_id;
	}

	
    
    

}
