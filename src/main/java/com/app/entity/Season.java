package com.app.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "season")
public class Season implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;
	
//	@OneToMany(mappedBy = "season")
//	private List<SeasonSerie> listSeasonSerie;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

//	public List<SeasonSerie> getListSeasonSerie() {
//		return listSeasonSerie;
//	}
//
//	public void setListSeasonSerie(List<SeasonSerie> listSeasonSerie) {
//		this.listSeasonSerie = listSeasonSerie;
//	}

	

	

}
