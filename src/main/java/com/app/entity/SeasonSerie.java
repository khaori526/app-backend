package com.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "season_serie")
@JsonIgnoreProperties({"serie", "season"})
public class SeasonSerie implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
    private SeasonSerieKey id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @MapsId("serie_id")
    @JoinColumn(name = "serie_id")
	@Transient
    private Serie serie;
 
	@ManyToOne(fetch = FetchType.LAZY)
    @MapsId("season_id")
    @JoinColumn(name = "season_id")
	@Transient
    private Season season;
    
    @Column
    private int episodes;

	public SeasonSerieKey getId() {
		return id;
	}

	public void setId(SeasonSerieKey id) {
		this.id = id;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public Season getSeason() {
		return season;
	}

	public void setSeason(Season season) {
		this.season = season;
	}

	public int getEpisodes() {
		return episodes;
	}

	public void setEpisodes(int episodes) {
		this.episodes = episodes;
	}

	
    
    

}
