package com.app.service;

import java.net.MalformedURLException;
import java.nio.file.Path;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface UploadFileService {
	
	public Resource cargar(String nombreFoto) throws MalformedURLException;
	public String copiar(MultipartFile archivo) throws java.io.IOException;
	public boolean eliminar(String nombreFoto);
	public Path getPath(String nombreFoto);

}
