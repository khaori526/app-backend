package com.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.app.entity.SeasonSerie;
import com.app.entity.Serie;
import com.app.repository.SerieRepository;
import com.app.service.SerieService;

@Service
public class SerieServiceImpl implements SerieService{
	
	@Autowired
	private SerieRepository serieRepository;

	@Override
	public List<Serie> findAll() {
		return serieRepository.findAll();
	}

	@Override
	public Serie findById(Integer id) {
		return serieRepository.findById(id).orElse(null);
	}

	@Override
	public Serie save(Serie serie) {
		return serieRepository.save(serie);
	}

	@Override
	public void delete(Serie serie) {
		serieRepository.delete(serie);
		
	}

	@Override
	public Page<Serie> findAll(Pageable pageable) {
		return serieRepository.findAll(pageable);
	}

	@Override
	public List<SeasonSerie> findSerieWithSeasons(Integer id) {
		return serieRepository.findSerieWithSeasons(id);
	}

}
