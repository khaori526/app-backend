package com.app.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.app.entity.SeasonSerie;
import com.app.entity.Serie;

public interface SerieService {
	
	public List<Serie> findAll();
	public Page<Serie> findAll(Pageable pageable);
	public Serie findById(Integer id);
	public Serie save(Serie serie);
	public void delete(Serie serie);
	
	public List<SeasonSerie> findSerieWithSeasons(Integer id);

}
