package com.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.entity.SeasonSerie;
import com.app.entity.Serie;

@Repository
public interface SerieRepository extends JpaRepository<Serie, Integer>{
	
	@Query("select ss from SeasonSerie ss where ss.id.serie_id = :idSerie")
	public List<SeasonSerie> findSerieWithSeasons(@Param(value = "idSerie") Integer id);

}
