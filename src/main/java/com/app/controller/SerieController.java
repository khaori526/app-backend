package com.app.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.entity.SeasonSerie;
import com.app.entity.Serie;
import com.app.service.SerieService;
import com.app.service.UploadFileService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/serie")
public class SerieController {
	
	@Autowired
	private SerieService serieService;
	
	@Autowired
	private UploadFileService uploadFileService;
	
	@GetMapping("/findAll")
	public ResponseEntity<?> findAll(){
		Map<String, Object> response = new HashMap<>();
		response.put("series", serieService.findAll());
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@GetMapping(path = "/findAll/page/{page}", produces = "application/json")
	public ResponseEntity<?> findAll(@PathVariable Integer page){
		Map<String, Object> response = new HashMap<>();
		Pageable pageable = PageRequest.of(page, 4);
		response.put("series", serieService.findAll(pageable));
		
		ObjectMapper mapper = new ObjectMapper();
    	try {
			String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response);
			System.out.println(json);
		} catch (JsonProcessingException e1) {
			
			e1.printStackTrace();
		}

    	
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@GetMapping("/ver/{id}")
	public ResponseEntity<?> show(@PathVariable Integer id) {
		
		Serie serie = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			serie = serieService.findById(id);
			if(serie == null) {
				response.put("mensaje", "La serie ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			}else {
				List<SeasonSerie> listSeasonSerie = serieService.findSerieWithSeasons(id);
				serie.setSeasons(listSeasonSerie);
				response.put("serie", serie);
			}
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		ObjectMapper mapper = new ObjectMapper();
    	try {
			String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response);
			System.out.println(json);
		} catch (JsonProcessingException e1) {
			
			e1.printStackTrace();
		}
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PostMapping("/create")
	public ResponseEntity<?> create(@Valid @RequestBody Serie serie, BindingResult result) {
		
		Serie serieNew = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			serieNew = serieService.save(serie);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "La serie ha sido creada con éxito!");
		response.put("serie", serieNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable Integer id) {
		
		Map<String, Object> response = new HashMap<>();
		
		try {
			Serie serie = serieService.findById(id);
			String nombreFotoAnterior = serie.getFoto();
			
			uploadFileService.eliminar(nombreFotoAnterior);
			
			serieService.delete(serie);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar la serie de la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "La serie ha sido eliminada con éxito!");
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@GetMapping("/uploads/img/{nombreFoto:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto){

		Resource recurso = null;
		
		try {
			recurso = uploadFileService.cargar(nombreFoto);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");
		
		return new ResponseEntity<Resource>(recurso, cabecera, HttpStatus.OK);
	}
	
	@PostMapping("/upload")
	public ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Integer id){
		Map<String, Object> response = new HashMap<>();
		
		Serie serie = serieService.findById(id);
		
		if(!archivo.isEmpty()) {

			String nombreArchivo = null;
			try {
				nombreArchivo = uploadFileService.copiar(archivo);
			} catch (IOException e) {
				response.put("mensaje", "Error al subir la imagen del cliente");
				response.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			String nombreFotoAnterior = serie.getFoto();
			
			uploadFileService.eliminar(nombreFotoAnterior);
						
			serie.setFoto(nombreArchivo);
			
			serieService.save(serie);
			
			response.put("serie", serie);
			response.put("mensaje", "Has subido correctamente la imagen: " + nombreArchivo);
			
		}
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Serie serie, BindingResult result, @PathVariable Integer id) {

		Serie serieActual = serieService.findById(id);

		Serie serieUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if (serieActual == null) {
			response.put("mensaje", "Error: no se pudo editar, la serie ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {

			serieActual.setTitle(serie.getTitle());
			serieActual.setDescription(serie.getDescription());
			serieActual.setPremiereDate(serie.getPremiereDate());

			serieUpdated = serieService.save(serieActual);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar el cliente en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "La serie ha sido actualizada con éxito!");
		response.put("serie", serieUpdated);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

}
